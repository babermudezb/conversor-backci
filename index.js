exports.handler = async (event) => {
    
    let uni_dat = event.unidat;
    let dat = Number(event.dat);
    let uni_conv = event.uniconv;
    let dat_conv = 0;
    
    if (uni_dat == uni_conv )
    {
        dat_conv=dat;
    }else{
        if (uni_dat == "Newtons"){
            if(uni_conv == "Dinas"){
                // conversion a dina
                dat_conv = dat * 100000;
            }else {
                // conversion a libra
                dat_conv = dat * 0.2248;
            }
        }
        if (uni_dat == "Dinas"){
            if(uni_conv == "Newtons"){
                //conversion a newton
                dat_conv = dat / 100000;
            }else {
                //conversion a libra
                dat_conv = dat * 0.0000022481;
            }
        }
        if (uni_dat == "Libras"){
            if(uni_conv == "Newtons"){
                //conversion a newton
                dat_conv = dat * 4.4482;
            }else {
                //conversion a dina
                dat_conv = dat * 444822.16;
            }
        }
    }
    // TODO implement
    const response = {
        
        statusCode: 200,
        dat: JSON.stringify(dat),
        uni_dat: uni_dat,
        dat_conv: JSON.stringify(dat_conv),
        uni_conv: uni_conv
    };
    //comentario AWS Lambda
    return response;
};
